class Visit < Sequel::Model
  include VisitReports

  plugin :auto_validations

  DIGEST_VALUES = %i(created_at url referrer)

  def before_validation
    super
    set_created_at
    set_day
    set_hashed
  end

  private

  def set_created_at
    self.created_at = Time.now
  end

  def set_day
    self.day = created_at_to_days if created_at
  end

  def set_hashed
    self.hashed = digested_values
  end

  def digested_values
    Digest::MD5.hexdigest(values.slice(*DIGEST_VALUES).to_s)
  end

  def created_at_to_days
    self.class.date_to_days(created_at)
  end
end
