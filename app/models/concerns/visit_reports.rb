module VisitReports
  extend ActiveSupport::Concern

  module ClassMethods
    def display_report(data)
      report = data.to_a.map do |item|
        item = item.values
        if item[:referrers]
          referrers = item[:referrers].split(",")
          referrers.map! do |ref|
            values = ref.split("||")
            out = {}
            out[:url] = values[0]
            out[:visits] = values[1].to_i
            out
          end
          item[:referrers] = referrers
        end
        item[:day] = days_to_date(item[:day]).to_date.to_s
        item[:visits] = item.delete(:count) if item[:count]
        item
      end
      report.group_by { |x| x[:day] }
    end

    def top_urls(days = 5)
      date = self.today_in_days
      where("day >= ?", date - days).
      where("day < ?", date).
        select_group(:day, :url).
        select_append{count(:id).as(:visits)}.
      order(Sequel.desc(:day), Sequel.desc(:visits))
    end

    # NOTE: I'd probably ask for some input here. Doing unions doesn't seem like
    # the best option but it gets the job done for now.
    def top_referrers(days = 5)
      memo = self.top_referrers_query(Time.now - days.days)
      days.times.to_a.reverse.each do |i|
        next if i == 0
        memo = memo.union(self.top_referrers_query(Time.now - i.days))
      end
      return memo
    end

    def top_referrers_query(date = Time.now)
      day = self.date_to_days(date)
      sql = <<-QUERY.strip_heredoc
        SELECT
          v1.day,
          v1.url,
          v2.visits,
          SUBSTRING_INDEX(GROUP_CONCAT(DISTINCT CONCAT(v1.referrer,'||',v1.referrer_count) ORDER BY v1.referrer_count desc SEPARATOR ','), ',', 5) AS referrers
        FROM
          (
            SELECT referrer, url, day, count(`id`) AS referrer_count
            FROM visits
            WHERE day = #{day}
            GROUP BY referrer, url, day
          ) AS v1
        LEFT JOIN 
          (
            SELECT url, day, count(`id`) AS visits
            FROM visits
            WHERE day = #{day}
            GROUP BY day, url
          ) AS v2 ON v1.url = v2.url AND v1.day = v2.day
        GROUP BY url, day
        ORDER BY day asc, visits desc, referrer_count desc
        LIMIT 10
      QUERY
      Visit.with_sql(sql)
    end

    def day_one
      @day_one ||= DateTime.parse("0001-01-01")
    end 

    def date_to_days(date)
      date = date.to_datetime if date.respond_to?(:to_datetime)
      (date - day_one).to_i
    end

    def days_to_date(n)
      day_one + n.days
    end

    def today
      self.days_to_date(self.today_in_days)
    end

    def today_in_days
      self.date_to_days(Time.now)
    end
  end
end
