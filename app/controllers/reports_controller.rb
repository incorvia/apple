class ReportsController < ApplicationController
  def top_referrers
    render json: Visit.display_report(Visit.top_referrers).as_json
  end

  def top_urls
    render json: Visit.display_report(Visit.top_urls).as_json
  end
end
