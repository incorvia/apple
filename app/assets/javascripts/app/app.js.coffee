@Apple = angular.module('Apple', ['ngRoute', 'templates'])

@Apple.config(($routeProvider, $locationProvider) ->
    $locationProvider.html5Mode({ enabled: true, requireBase: true })
    $routeProvider
      .when '/',
        templateUrl: 'home'
      .when '/reports/top_urls',
        templateUrl: 'top_urls'
        controller: "TopUrlsController"
      .when '/reports/top_referrers',
        templateUrl: 'top_referrers'
        controller: "TopReferrersController"
)

@Apple.controller "TopReferrersController", ($scope, $routeParams) ->
  $scope.title = "Top Referrers"
  
  $.getJSON "/top_referrers", ( data ) ->
    $scope.data = data
    $scope.$apply()

@Apple.controller "TopUrlsController", ($scope) ->
  $scope.title = "Top Urls"

  $.getJSON "/top_urls", ( data ) ->
    $scope.data = data
    $scope.$apply()
