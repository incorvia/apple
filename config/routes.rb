Rails.application.routes.draw do
  root to: 'reports#app'

  get '/reports/:report', to: 'reports#app'

  get '/top_urls', to: 'reports#top_urls'
  get '/top_referrers', to: 'reports#top_referrers'

end
