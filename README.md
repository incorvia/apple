# README

## About

This is a sample app I developed for the Apple Documentation team to demonstrate my ability to prototype an analytics application.
I'm happy that I completed the minimal requirements, but obviously would have loved to improve this farther (see "Things I'd Improve").

Using Sequel was a new experience which I did enjoy as well as using MySQL as I basically have only used Postgres to this point.  I found working without window functions in MySQL to be a challenge but hacked my way around the problem regardless and no doubt could improve on my implementation of the data fetching.  I would personally prefer in production to use a time-series database, Postgres, or something like ElasticSearch with more advanced querying functionality but was happy to use the recommended technologies to solve this problem.

## Getting up and running

    rake db:create
    rake db:migrate
    rake db:seed
    rails s

## Things I'd Improve

A few things I'd improve if I had more time:

* Speed up top_referrers query

* Add spinner via angular http interceptors.

* Integration specs on the front end.

* Improve UI display. Right now I sorta just hacked the nested list on the top_referrers report.

* Data only changes once a day since it's retrospective. Cache daily. Much faster!
