Sequel.migration do
  change do
    add_index :visits, [:day, :url]
  end
end
