Sequel.migration do
  change do
    add_column :visits, :day, :integer, null: false
    add_index :visits, :day
    add_index :visits, [:url, :day]
  end
end
