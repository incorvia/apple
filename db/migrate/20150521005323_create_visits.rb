Sequel.migration do
  up do
    transaction do
      create_table :visits do
        primary_key :id
        String :url, null: false
        String :referrer
        DateTime :created_at, null: false
        String :hashed, null: false
      end

      add_index :visits, :created_at
      add_index :visits, :hashed
      add_index :visits, :url
      add_index :visits, :referrer
    end
  end

  down do
    drop_table :visits
  end
end
