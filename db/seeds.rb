# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

module SeedHelper
  URL_SCHEMES   = %w(http https)
  URL_PATHS     = %w(ios osx xcode swift objective-c)
  URL_HOSTS     = %w(apple.com www.apple.com developer.apple.com en.wikipedia.org opensource.org)
  URL_REFERRERS = URL_HOSTS + [nil]

  @number_of_days = (ENV['DAYS'] || 100).to_i

  if @number_of_days < 10
    @number_of_days = 10
  elsif @number_of_days > 1000
    @number_of_days = 1000
  end

  def self.build_scheme
    URL_SCHEMES.sample
  end

  def self.build_path(i)
    i % 100 == 0 ? nil : "/#{URL_PATHS.sample}/document-#{rand(10)}.html"
  end

  def self.build_host
    URL_HOSTS.sample
  end

  def self.build_url(i)
    URI::Generic.new(self.build_scheme, nil, self.build_host, nil, nil, self.build_path(i), nil, nil, nil, nil, nil).to_s
  end

  def self.date_helper(i)
    Time.now - (i / (VISITS / @number_of_days).truncate).days + rand(24 * 60).minutes
  end
end

VISITS = 1000 * 1000

pb = ProgressBar.create(title: "Loading Visits", total: VISITS, format: "%t (%e | Rate: %r/s): %w")

inserts = []

VISITS.times do |i|
  ref   = SeedHelper.build_url(i)
  url   = SeedHelper.build_url(i)
  date  = SeedHelper.date_helper(i)
  visit = Visit.new(url: url, referrer: ref, created_at: date)

  def visit.set_created_at; nil; end;

  visit.before_validation

  inserts << visit

  if (i % 10000 == 0) || (i == (VISITS - 1))
    Visit.multi_insert(inserts)
    pb.progress = i
    inserts = []
  end
end
