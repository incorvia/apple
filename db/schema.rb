Sequel.migration do
  change do
    create_table(:schema_migrations) do
      column :filename, "varchar(255)", :null=>false
      
      primary_key [:filename]
    end
    
    create_table(:visits) do
      primary_key :id, :type=>"int(11)"
      column :url, "varchar(255)", :null=>false
      column :referrer, "varchar(255)"
      column :created_at, "datetime", :null=>false
      column :hashed, "varchar(255)", :null=>false
      column :day, "int(11)", :null=>false
      
      index [:created_at]
      index [:day]
      index [:day, :url]
      index [:hashed]
      index [:referrer]
      index [:url, :day]
      index [:url]
    end
  end
end
              Sequel.migration do
                change do
                  self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150521005323_create_visits.rb')"
self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150521200940_add_date_cache_to_visit.rb')"
self << "INSERT INTO `schema_migrations` (`filename`) VALUES ('20150526124443_add_days_url_index_to_visits.rb')"
                end
              end
