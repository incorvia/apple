require 'rails_helper'

RSpec.describe ReportsController, type: :controller do

  describe "GET #top_referrers" do
    it "returns http success" do
      get :top_referrers
      expect(response).to have_http_status(:success)
    end

    it 'returns a formatted json response' do
      Visit.multi_insert([{ referrer: 'test_ref', day: Visit.today_in_days - 1, created_at: Time.now - 1.days, hashed: 1, url: 'test_url' }])
      get :top_referrers
      out = JSON.parse(response.body)
      expect(out[(Visit.today - 1.days).to_date.to_s]).to eql([{"day"=>"2015-05-25", "url"=>"test_url", "visits"=>1, "referrers"=>[{"url"=>"test_ref", "visits"=>1}]}])
    end
  end

  describe "GET #top_urls" do
    it "returns http success" do
      get :top_urls
      expect(response).to have_http_status(:success)
    end

    it 'returns a formatted json response' do
      Visit.multi_insert([{ referrer: 'test_ref', day: Visit.today_in_days - 1, created_at: Time.now - 1.days, hashed: 1, url: 'test_url' }])
      get :top_urls
      out = JSON.parse(response.body)
      expect(out[(Visit.today - 1.days).to_date.to_s]).to eql([{"day"=>"2015-05-25", "url"=>"test_url", "visits"=>1}])
    end
  end
end
