require 'rails_helper'

RSpec.describe "VisitReports" do
  describe 'reports' do
    before(:each) do
      2.times do |i|
        days_ago = i + 1
        Visit.multi_insert([
          { referrer: 'test_ref', day: Visit.today_in_days - days_ago, created_at: Time.now - days_ago.days, hashed: 1, url: 'test_url' },
          { referrer: 'test_ref', day: Visit.today_in_days - days_ago, created_at: Time.now - days_ago.days, hashed: 2, url: 'test_url' },
          { referrer: 'test_ref', day: Visit.today_in_days - days_ago, created_at: Time.now - days_ago.days, hashed: 2, url: "test_url_#{i}" }
        ])
      end
    end

    describe '.top_urls' do
      it 'returns a list of the top urls' do
        values = Visit.top_urls.to_a.map(&:values)
        expect(values.length).to eql(4)
        expect(values[0]).to eql({day: Visit.today_in_days - 1, url: "test_url", visits: 2})
      end
    end

    describe '.top_referrers' do
      it 'returns a list of the top urls' do
        values = Visit.top_referrers.to_a.map(&:values)
        expect(values.length).to eql(4)
        expect(values[0]).to eql({ day: Visit.today_in_days - 2, url: "test_url", visits: 2, referrers: "test_ref||2"})
      end

      it 'returns multiple days' do
        values = Visit.top_referrers.to_a
        expect(values.map { |x| x.values[:day] }.uniq.length).to eql(2)
      end
    end

    describe '.top_referrers_query' do
      it 'returns a list of the top urls' do
        values = Visit.top_referrers_query(Time.now - 1.days).to_a.map(&:values)
        expect(values.length).to eql(2)
        expect(values[0]).to eql({ day: Visit.today_in_days - 1, url: "test_url", visits: 2, referrers: "test_ref||2"})
      end
    end
  end

  describe '.display_report'do
    it 'groups results by date' do
      query = [OpenStruct.new(values: { day: 1, value: 1 }), OpenStruct.new(values: { day: 1, value: 1 })]
      expect(Visit.display_report(query)).to eql({"0001-01-02"=>[{day: "0001-01-02", value: 1}, {day: "0001-01-02", value: 1}]})
    end
  end

  describe '.today' do
    it 'returns todays date in number of days' do
      allow(Time).to receive(:now).and_return(Time.parse("0002-01-01"))
      expect(Visit.today.to_date.to_s).to eql("0002-01-01")
    end
  end

  describe '.days_to_date' do
    it 'converts a number of days to a date' do
      out = Visit.days_to_date(Visit.date_to_days(DateTime.parse("2015-01-01")))
      expect(out.to_date.to_s).to eql("2015-01-01")
    end
  end
end

