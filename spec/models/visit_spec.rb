require 'rails_helper'

RSpec.describe Visit, type: :model do
  let(:visit) { Visit.new }

  describe '#validate' do
    describe 'hashed' do
      before(:each) do
        allow(visit).to receive(:set_hashed).and_return(nil)
      end

      it 'does not allow nil values' do
        visit.hashed = nil
        visit.valid?
        expect(visit.errors[:hashed].length).to be > 0
      end

      it 'allows hashed to be set' do
        visit.hashed = Time.now
        visit.valid?
        expect(visit.errors[:hashed]).to eql(nil)
      end
    end

    describe 'created_at' do
      before(:each) do
        allow(visit).to receive(:set_created_at).and_return(nil)
      end

      it 'does not allow nil values' do
        visit.created_at = nil
        visit.valid?
        expect(visit.errors[:created_at].length).to be > 0
      end

      it 'allows created_at to be set' do
        visit.created_at = Time.now
        visit.valid?
        expect(visit.errors[:created_at]).to eql(nil)
      end
    end

    describe 'day' do
      before(:each) do
        allow(visit).to receive(:set_day).and_return(nil)
      end

      it 'does not allow nil values' do
        visit.day = nil
        visit.valid?
        expect(visit.errors[:day].length).to be > 0
      end

      it 'allows day to be set' do
        visit.day = Time.now
        visit.valid?
        expect(visit.errors[:day]).to eql(nil)
      end
    end

    describe 'url' do
      describe 'max_length' do
        it 'allows urls shorter than 2083 characters' do
          visit.url = "0" * 255
          visit.valid?
          expect(visit.errors[:url]).to eql(nil)
        end

        it 'disallows urls longer than 2083 characters' do
          visit.url = "0" * 256
          visit.valid?
          expect(visit.errors[:url].length).to be > 0
        end

        it 'allows nil values' do
          visit.url = 'http://www.apple.com'
          visit.valid?
          expect(visit.errors[:url]).to eql(nil)
        end

        it 'disallows nil values' do
          visit.url = nil
          visit.valid?
          expect(visit.errors[:url].length).to be > 0
        end
      end
    end

    describe 'referrer' do
      describe 'max_length' do
        it 'allows referrers shorter than 2083 characters' do
          visit.referrer = "0" * 255
          visit.valid?
          expect(visit.errors[:referrer]).to eql(nil)
        end

        it 'disallows referrers longer than 2083 characters' do
          visit.referrer = "0" * 256
          visit.valid?
          expect(visit.errors[:referrer].length).to be > 0
        end

        it 'allows nil values' do
          visit.referrer = nil
          visit.valid?
          expect(visit.errors[:referrer]).to eql(nil)
        end
      end
    end

    describe '#before_validations' do
      describe 'set_created_at' do
        it 'sets the current time in the created_at field' do
          allow(Time).to receive(:now).and_return('now!')
          expect(visit.created_at).to eql(nil)
          visit.send(:set_created_at)
          expect(visit.created_at).to eql('now!')
        end
      end

      describe 'set_day' do
        it 'sets the current time in the created_at field' do
          visit.created_at = DateTime.parse("0002-01-01")
          expect(visit.day).to eql(nil)
          visit.send(:set_day)
          expect(visit.day).to eql(365)
        end
      end

      describe 'set_hashed' do
        it 'creates an MD5 hexdigest of the values' do
          visit.id = 1
          visit.url = 'test_url'
          visit.referrer = 'test_url'
          visit.created_at = 'now!'
          allow(Digest::MD5).to receive(:hexdigest).
            with({ created_at: 'now!', url: 'test_url', referrer: 'test_url' }.to_s).
            and_return('digested!')
          visit.send(:set_hashed)
          expect(visit.hashed).to eql('digested!')
        end
      end
    end
  end
end
